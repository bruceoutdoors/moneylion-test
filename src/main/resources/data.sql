CREATE TABLE users (
    id INT AUTO_INCREMENT  PRIMARY KEY,
    email VARCHAR(250) NOT NULL
);

CREATE UNIQUE INDEX email_idx ON users (email);

CREATE TABLE features (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  feature_name VARCHAR(250) NOT NULL
);

CREATE UNIQUE INDEX feature_name_idx ON features (feature_name);

CREATE TABLE user_feature (
  user_id INT NOT NULL,
  feature_id INT NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (feature_id) REFERENCES features(id)
);

CREATE PRIMARY KEY user_feature_pk ON user_feature (user_id, feature_id);

-- Seed data...
--INSERT INTO users (id, email) VALUES
--(1, 'covid-19@gmail.com');
--
--INSERT INTO features (id, feature_name) values
--(1, 'Dry Cough'),
--(2, 'Fever'),
--(3, 'Bloody Diarrhea'),
--(4, 'Zombification');

