package com.moneylion.techassesment;

public class FeatureGetResponse {
    private final boolean canAccess;

    public FeatureGetResponse(boolean canAccess) {
        this.canAccess = canAccess;

    }
    public boolean getCanAccess() {
        return this.canAccess;
    };
}
