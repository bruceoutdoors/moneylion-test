package com.moneylion.techassesment;

import java.sql.ResultSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class TechAssesmentApplication {
	@Autowired
	JdbcTemplate jdbc;

	public static void main(String[] args) {
		SpringApplication.run(TechAssesmentApplication.class, args);
	}

	@GetMapping("/feature")
	public FeatureGetResponse getFeature(
			@RequestParam(value = "email")
					String email,
			@RequestParam(value = "featureName")
					String featureName) {

		Boolean exists = jdbc.query(
				String.join("\n"
						, "SELECT 1 FROM user_feature uf"
						, "INNER JOIN users u"
						, "ON uf.user_id = u.id"
						, "AND u.email = ?"
						, "INNER JOIN features f"
						, "ON uf.feature_id = f.id"
						, "AND f.feature_name = ?"),
				new Object[]{ email, featureName },
				ResultSet::next
				);

		return new FeatureGetResponse(exists);
	}

	@PostMapping("/feature")
	public ResponseEntity setFeature(
			@RequestParam(value = "email")
					String email,
			@RequestParam(value = "featureName")
					String featureName,
			@RequestParam(value = "enable")
					Boolean enable) {

		// Linking a user with a feature, adding a user, and adding a feature should be 3 separate API calls, but for a test this suffice.
		HttpStatus response = HttpStatus.OK;

		// Ideally it would be better to figure out without a separate query, it's easy to do in postgres; H2 I'm not so sure.
		boolean canAccess = this.getFeature(email, featureName).getCanAccess();

		if (canAccess == enable) {
			response = HttpStatus.NOT_MODIFIED;
		} else if (!canAccess && enable) {
			// TODO: figure out how to do named parameters as with query method. Code below is prone to SQL injection
			jdbc.execute("MERGE INTO users (email) KEY (email) VALUES ('" + email + "');");
			jdbc.execute("MERGE INTO features (feature_name) KEY (feature_name) VALUES ('" + featureName + "')");
			jdbc.execute(String.join("\n"
					, "MERGE INTO user_feature AS uf"
					, "USING ("
					, "		SELECT u.id user_id, f.id feature_id FROM users u"
					, "		INNER JOIN features f"
					, "		ON f.feature_name = '" + featureName + "'"
					, "		AND u.email = '" + email + "'"
					, ") AS s"
					, "ON (uf.user_id = s.user_id AND uf.feature_id = s.feature_id)"
					, "WHEN NOT MATCHED THEN"
					, "INSERT (user_id, feature_id) VALUES"
					, "		(s.user_id, s.feature_id);"));
		} else {
			// Does not remove the user and feature, just the link between them
			jdbc.execute(String.join("\n"
					, "DELETE FROM user_feature"
					, "WHERE feature_id = (SELECT id FROM features WHERE feature_name = '" + featureName + "')"
					, "	 AND user_id = (SELECT id FROM users WHERE email = '" + email + "')"));
		}

		return new ResponseEntity<>("", response);
	}
}

