package com.moneylion.techassesment;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class TechAssesmentApplicationTests {
	@Autowired
	private TechAssesmentApplication app;

	@Test
	void featureTests() {
		FeatureGetResponse res = app.getFeature("covid-19@gmail.com", "Fever");
		assertThat(res.getCanAccess()).isEqualTo(false);

		ResponseEntity<String> postRes = app.setFeature("covid-19@gmail.com", "Fever", true);
		assertThat(postRes.getStatusCode()).isEqualTo(HttpStatus.OK);

		postRes = app.setFeature("covid-19@gmail.com", "Fever", true);
		assertThat(postRes.getStatusCode()).isEqualTo(HttpStatus.NOT_MODIFIED);

		res = app.getFeature("covid-19@gmail.com", "Fever");
		assertThat(res.getCanAccess()).isEqualTo(true);

		postRes = app.setFeature("covid-19@gmail.com", "Fever", false);
		assertThat(postRes.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

}
