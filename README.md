# MoneyLion Tech Assesment

# Getting Started

```
mvn spring-boot:run
```

Or attach debugger on port `5005`:

```
mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005"
```

Run unit tests:

```
mvn test
```

